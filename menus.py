from menu import *
from fn import *
from manageTableMenus import *
import click

class HelpMenu(Menu):

    def Start(self)-> bool:
        print(f"""
====================================================================
    
    {self.menuName} Menu - MYSQL CLI FAKE DATABASE

    some bugs may occur according to OS you are using (this project only support python 3.8.5, mac OS).
    you can create table by pressing t button on your keyboard
    

    - (q) go back to main menu
    - (x) to force quit python

====================================================================
        """)

        key: str

        while True:
            key = click.getchar()

            if key == 'x':
                exit()
            elif key == 'q':
                break
        
        # outside while loop
        if key == 'q' or key == 'Q':
            return True

class ManageTableMenu(Menu):

    def Start(self)-> bool:

        key: str
        
        result = self.childrenMenu(manageTableMenus)

        return result

    def printMainMenu(self):
        print(f"""
====================================================================
    
    {self.menuName} Menu - MYSQL CLI FAKE DATABASE

    - (c) create table
    - (l) show list of tables
    - (i) info about this feature
    - (d) delete table

    ---------------------------

    - (q) go back to main menu
    - (x) to force quit python

====================================================================
        """)



helpMenu = HelpMenu("help", 'h')
manageTableMenu = ManageTableMenu("Table", 't')

menus = [helpMenu,manageTableMenu]