import os
import json

readDBFile = open('data.json', 'r')
database: dict = json.loads(readDBFile.read()) # dict type

def clear_screen():
    os.system('clear')

def forceQuit():
    clear_screen()
    print('Thank you for playing with this fake mysql database CLI!')
    exit()

def printMainMenu():
    print("""
====================================================================
    
    Welcome to MYSQL FAKE DATABASE CLI BUILT WITH PYTHON

    - (t) manage tables
    - (c) manage collections
    - (p) mysql playgrounds
    - (h) help
    - (q) to force quit python

====================================================================
    """)

yesInput = ['yes', 'y', 'Y', 'YES']
noInput = ['no', 'n', 'N', 'NO']
ynInput = [*yesInput, *noInput]
