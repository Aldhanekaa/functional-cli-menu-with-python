import click
from fn import *

class Menu:
    def __init__(self, menuName: str, key: str): 
        self.menuName = menuName
        self.key = key

    def Start(self):
        pass

    def printMainMenu(self):
        pass

    def childrenMenu(self,menus:list):
        self.printMainMenu()
        
        while(True):
            found: bool = False
            key = click.getchar()

            if not found:
                clear_screen()
                self.printMainMenu()

            # if user pressed quit key event
            if (key == 'q' or key == 'Q' or key == 'x' or key == 'X'):
                if (key == 'q' or key == 'Q'):
                    return True
                forceQuit()

            elif type(menus) is list:
                for menu in menus:
                    if menu.key == key:
                        found = True
                        clear_screen()
                        result = menu.Start()

                        # if function returns true
                        if result:
                            clear_screen()
                            self.printMainMenu()

                    else:
                        if not found:
                            found = False