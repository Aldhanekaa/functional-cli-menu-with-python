from menu import *
from fn import *
import click
import time
import functools


class HelpMenu(Menu):

    def Start(self)-> bool:
        print(f"""
====================================================================
    
    {self.menuName} Menu - MYSQL CLI FAKE DATABASE

    - press [q] key to go back)
    - press [x] key to quit CLI)

====================================================================
        """)
        


        key: str

        while True:
            key = click.getchar()

            if key == 'x':
                exit()
            elif key == 'q':
                break
        
        # outside while loop
        if key == 'q' or key == 'Q':
            return True


class ShowTables(Menu):

    def showTables(self):
        tables: list = database.get("table", 0)

        [longestTableNameWord, longestTableId] = self.longestWord(tables)

        print("=" * (longestTableNameWord + longestTableId))

        print("|",  end=" ")
        print("name", end=" ")
        print(" " * (longestTableNameWord-4), end=" ")

        print("| id", end=" ")
        print(" " * (longestTableId-2), end=" |")

        print(" collections", end=" ")
        print(" " * (longestTableId-2), end=" |")

        print(" " * (longestTableNameWord + 7))

        for table in tables:
            name = table["name"]
            id = str(table["id"])
            nameLeng = len(name)
            idLeng = len(id)

            # print(nameLeng, longestTableNameWord)
            print("|", end=" ")
            print(name, end=" ")
            print(" " * (longestTableNameWord-nameLeng), end=" | ")
            print(id, end=" ")
            print(" " * (longestTableId-idLeng), end=" |\n")

        print("=" * (longestTableNameWord + longestTableId))

        print("\npress `q` to go back")

        return

    def longestWord(self, words: list)->int:
        wordLeng: int = 0
        idLeng: int = 0

        for i in words:
            word = i['name']
            id = str(i['id'])

            if len(word) > wordLeng:
                wordLeng = len(word)

            if len(id) > idLeng:
                idLeng = len(id)

        return [wordLeng,idLeng]



    def Start(self)-> bool:

        self.showTables()

        key: str

        while True:
            key = click.getchar()

            if key == 'x':
                exit()
            elif key == 'q':
                break
        
        # outside while loop
        if key == 'q' or key == 'Q':
            return True
    
    def preview(self):
        print(f"""
====================================================================
    
    {self.menuName} Menu - MYSQL CLI FAKE DATABASE

    - (q) go back to manage tables menu
    - (x) to force quit python

====================================================================
        """)

class CreateTable(Menu):
    def createTable(self):
        tables: list = database.get("table", 0)

        IsThere: bool = False

        while (True):

            if (IsThere):
                clear_screen()
                print("This Name Already Registered Please Use Other")
            else:
                clear_screen()
                print("What's Table Name?")

            tableName = str(input("> "))

            try:
                isThere = next(item for item in tables if item["name"] == tableName)
                IsThere = True
                continue

            except:
                tables.append({'name': tableName, 'id': str(time.time())})
                break

        database.update(tables)
        # print(database)

        dbFile = open('data.json', 'w')
        dbFile.write(json.dumps(database))
        clear_screen()
        print("successfully create table")
        return


    def While(self):
        error: int = 0

        while(True):

            if (error == 0):
                print("Do you want to create table? ")
            else:
                clear_screen()
                print("Y or N?")

            wannaCreateTable = str(input("> "))

            if wannaCreateTable in ynInput:
                if wannaCreateTable in yesInput:
                    self.createTable()
                    self.Start()
                    pass
                else:
                    break

            else:
                if (wannaCreateTable == 'quit'):
                    break
                else:
                    error += 1
                    continue

        return

    def Start(self)-> bool:
        self.preview()

        self.While()

        key: str

        while True:
            key = click.getchar()

            if key == 'x':
                exit()
            elif key == 'q':
                break
        
        # outside while loop
        if key == 'q' or key == 'Q':
            return True
    
    def preview(self):
        print(f"""
====================================================================
    
    {self.menuName} Menu - MYSQL CLI FAKE DATABASE

    - (q) go back to manage tables menu
    - (x) to force quit python

====================================================================
        """)


helpMenu = HelpMenu("Informations about manage Tables", 'i')
createTableMenu = CreateTable("Create Table", 'c')
showTables = ShowTables('Show Tables', 'l')
manageTableMenus: list = [helpMenu,createTableMenu,showTables]