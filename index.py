import click
from tkinter import * 
import curses
from fn import *
from menus import *

clear_screen()

print("""
====================================================================

    Welcome to MYSQL FAKE DATABASE CLI BUILT WITH PYTHON

    made by Aldhanekaa (https://github.com/Aldhanekaa)

    press key 'q' to quit, otherwise press any key to start CLI 

====================================================================
""")

key = click.getchar()

if key == 'q' or key == 'Q':
    forceQuit()

clear_screen()

print("""
SUCCESSFULLY RUN MYSQL DATABASE

this database runs on port 8745
http://localhost:8745/
""")


printMainMenu()


while(True):
    found: bool = False
    key = click.getchar()

    if not found:
        clear_screen()
        printMainMenu()

    # if user pressed quit key event
    if (key == 'q' or key == 'Q'):
        forceQuit()
    else:
        found = False

        for menu in menus:
            if menu.key is key:
                found = True
                clear_screen()
                result = menu.Start()

                # if function returns true
                if result:
                    clear_screen()
                    printMainMenu()

            else:
                if not found:
                    found = False

databaseFile.close()